
#Stap 1: een IPv4-hostadres opvragen + Stap 2: een subnetmasker opvragen

def ask_for_number_sequence(message):
    Adressen = input(message + "\n")
    return [int(adres) for adres in Adressen.split(".")]

#Deel 2 -> IP controleren

def is_valid_ip_address(numberlist):
    Geldige_Input = True
    if len(numberlist) == 4:
        for IP_opgesplitst in numberlist:
            opgesplitst_IP = int(IP_opgesplitst)
            if not (opgesplitst_IP >= 0 and opgesplitst_IP <= 255):
                Geldige_Input = False
    else:
        Geldige_Input = False
    return Geldige_Input

#Stap 3: Geldigheid controleren van een adres

def is_valid_netmask(numberlist):
    Correct_Netmask = False
    checking_ones = True
    binary_netmask = ""
    if len(numberlist) == 4:
        Correct_Netmask = True
        for net_sep in numberlist:
            binary_netmask =  binary_netmask + f"{int(net_sep):08b}"
           
        for bit in binary_netmask:          
            if checking_ones ==  True and bit == "1":
                Correct_Netmask = True
            elif bit == "0" and checking_ones == True:
                checking_ones = False
                Correct_Netmask = True
            elif checking_ones == False and bit == "1":
                Correct_Netmask = False
            
    return Correct_Netmask

#Stap 4: aantal 1-bits tellen



def one_bits_in_netmask(Numlist):
    binary_netmask = ""
    counter = 0
    for bit in Numlist:
            binary_netmask += f"{int(bit):08b}"
        
    for bit2 in binary_netmask:
        if int(bit2) == 1:
            counter += 1

    return counter 

#Stap 5: adres van het subnet berekenen



def apply_network_mask(host_address, netmask):
    Binaire_subnetmask = []
    Getal1 = 0
    Getal2 = 0
    Getal3 = 0
    i = 0

    while i < 4:
        Getal1 = f"{int(host_address[i]):08b}"
        Getal2 = f"{int(netmask[i]):08b}"
        Getal3 = f"{int(Getal1,2) & int(Getal2,2):08b}"
        Binaire_subnetmask.append(f"{int(Getal3,2)}")
        i = i + 1
    Getal3=Binaire_subnetmask[0]+"."+Binaire_subnetmask[1]+"."+Binaire_subnetmask[2]+"."+Binaire_subnetmask[3]

    return(Getal3)

#Stap 6: wildcardmasker berekenen


def netmask_to_wildcard_mask(Subnetmask):
    wildcard_masker = []
    for element in Subnetmask:
        wildcard_element = ""
        for bit in f"{int(element):08b}":
            if bit == "0":
                wildcard_element += "1"
            else:
                wildcard_element += "0"
        wildcard_element = int(wildcard_element, 2)
        wildcard_masker.append(wildcard_element)

    return(wildcard_masker)

#Stap 7: broadcastadres berekenen


def get_broadcast_address(network_address, wildcard_mask):

    byte_1 = 0

    byte_2 = 0

    byte_3 = 0

    Bytes = 0

    binair_nieuw = []

    while Bytes < 4:

        byte_1 = f"{int(network_address[Bytes]):08b}"
        byte_2 = f"{int(wildcard_mask[Bytes]):08b}"
        byte_3 = f"{int(byte_1,2) | int(byte_2,2):08b}"
        binair_nieuw.append(f"{int(byte_3,2)}")
        Bytes = Bytes + 1
    byte_3 = binair_nieuw[0]+"."+binair_nieuw[1]+"."+binair_nieuw[2]+"."+binair_nieuw[3]

    return(byte_3)


#Stap 8: maximaal aantal hosts berekenen



def prefix_length_to_max_hosts(numbers_netmask):
    maximale_hosts = pow(2, numbers_netmask) - 2
    return(int(maximale_hosts))


ip_address = ask_for_number_sequence("Wat is het IP-adres?")
netmask_numbers = ask_for_number_sequence("Wat is het subnetmask?")


if is_valid_ip_address(ip_address) and is_valid_netmask(netmask_numbers):
    print("IP-adres en subnetmasker zijn geldig.")
else:
    import sys
    print("IP-adres en/of subnetmasker is ongeldig.")
    sys.exit(0)


netmask_length = one_bits_in_netmask(netmask_numbers)


print("De lengte van het subnetmasker is " + str(netmask_length))


print("Het adres van het subnet is " + str(apply_network_mask(ip_address, netmask_numbers)))


wildcardmasker = netmask_to_wildcard_mask(netmask_numbers)



print("Het wildcardmasker is " + str(wildcardmasker[0]) + "." + str(wildcardmasker[1]) + "." + str(wildcardmasker[2]) + "." + str(wildcardmasker[3]))


print("Het broadcastadres is " + str(get_broadcast_address(ip_address, wildcardmasker)))


print("Het maximaal aantal hosts op dit subnet is " + str(prefix_length_to_max_hosts(32-netmask_length)))