﻿Import-Module MicrosoftTeams
$credential = Get-Credential

Connect-MicrosoftTeams -Credential $credential

Get-TeamChannel -GroupId 09166e5a-31e5-4c11-b8f8-9771e1fe3468 | Write-Output

$rows = Import-Csv "D:\DigitaleSystemenEnNetwerken\semester2\OSscripting\project1powershell\channels.csv"

foreach ($row in $rows)
{
    New-TeamChannel -GroupId 09166e5a-31e5-4c11-b8f8-9771e1fe3468 -DisplayName $row.Displaynaam -Description $row.Omschrijving
}