
#Deel 1

def ask_for_number_sequence(message):
    Adressen = input(message + "\n")
    return [int(adres) for adres in Adressen.split(".")]

#Deel 2 -> IP controleren

def is_geldig_ip_adres(nummerlijst):
    Geldige_Input = True
    if len(nummerlijst) == 4:
        for IP_opgesplitst in nummerlijst:
            opgesplitst_IP = int(IP_opgesplitst)
            if not (opgesplitst_IP >= 0 and opgesplitst_IP <= 255):
                Geldige_Input = False
    else:
        Geldige_Input = False
    return Geldige_Input

#Deel 3: Controleren netmask

def is_geldig_netmask(nummerlijst):
    Correct_Netmask = False
    checking_ones = True
    binair_netmask = ""
    if len(nummerlijst) == 4:
        Correct_Netmask = True
        for net_sep in nummerlijst:
            binair_netmask =  binair_netmask + f"{int(net_sep):08b}"
           
        for bit in binair_netmask:          
            if checking_ones ==  True and bit == "1":
                Correct_Netmask = True
            elif bit == "0" and checking_ones == True:
                checking_ones = False
                Correct_Netmask = True
            elif checking_ones == False and bit == "1":
                Correct_Netmask = False
            
    return Correct_Netmask


def one_bits_in_netmask(Numlist):
    binair_netmask = ""
    counter = 0
    for bit in Numlist:
            binair_netmask += f"{int(bit):08b}"
        
    for bit2 in binair_netmask:
        if int(bit2) == 1:
            counter += 1

    return counter 

#Deel 4

def apply_network_mask(host_address, netmask):
    Binaire_subnetmask = []
    Getal1 = 0
    Getal2 = 0
    Getal3 = 0
    i = 0

    while i < 4:
        Getal1 = f"{int(host_address[i]):08b}"
        Getal2 = f"{int(netmask[i]):08b}"
        Getal3 = f"{int(Getal1,2) & int(Getal2,2):08b}"
        Binaire_subnetmask.append(f"{int(Getal3,2)}")
        i = i + 1
    Getal3=Binaire_subnetmask[0]+"."+Binaire_subnetmask[1]+"."+Binaire_subnetmask[2]+"."+Binaire_subnetmask[3]

    return(Getal3)

#Deel 5

def netmask_to_wildcard_mask(Subnetmask):
    wildcard_masker = []
    for element in Subnetmask:
        wildcard_element = ""
        for bit in f"{int(element):08b}":
            if bit == "0":
                wildcard_element += "1"
            else:
                wildcard_element += "0"
        wildcard_element = int(wildcard_element, 2)
        wildcard_masker.append(wildcard_element)

    return(wildcard_masker)

#Deel 6

def get_broadcast_address(NetworkAddr, wildcard_mask):

    byte_1 = 0

    byte_2 = 0

    byte_3 = 0

    Bytes = 0

    binair_nieuw = []

    while Bytes < 4:

        byte_1 = f"{int(NetworkAddr[Bytes]):08b}"
        byte_2 = f"{int(wildcard_mask[Bytes]):08b}"
        byte_3 = f"{int(byte_1,2) | int(byte_2,2):08b}"
        binair_nieuw.append(f"{int(byte_3,2)}")
        Bytes = Bytes + 1
    byte_3 = binair_nieuw[0]+"."+binair_nieuw[1]+"."+binair_nieuw[2]+"."+binair_nieuw[3]

    return(byte_3)


def prefix_length_to_max_hosts(nummers_netmask):
    maximale_hosts = pow(2, nummers_netmask) - 2
    return(int(maximale_hosts))


ip_address = ask_for_number_sequence("Wat is het IP-adres?")
netmask_nummers = ask_for_number_sequence("Wat is het subnetmask?")


if is_geldig_ip_adres(ip_address) and is_geldig_netmask(netmask_nummers):
    print("IP-adres en subnetmasker zijn geldig.")
else:
    import sys
    print("IP-adres en/of subnetmasker is ongeldig.")
    sys.exit(0)


netmask_length = one_bits_in_netmask(netmask_nummers)


print("Lengte subnetmasker: " + str(netmask_length))


print("subnet adres: " + str(apply_network_mask(ip_address, netmask_nummers)))


wildcardmasker = netmask_to_wildcard_mask(netmask_nummers)


print("Wildcardmasker: " + str(wildcardmasker[0]) + "." + str(wildcardmasker[1]) + "." + str(wildcardmasker[2]) + "." + str(wildcardmasker[3]))

print("Broadcastadres: " + str(get_broadcast_address(ip_address, wildcardmasker)))

print("Max hosts van dit subnet: " + str(prefix_length_to_max_hosts(32-netmask_length)))